Hello and welcome to our ray tracer.
This tracer was made by Tariq Bakhtali and Merijn Schepers

Contact info:
Tariq Bakhtali, 5631394, t.i.bakhtali@students.uu.nl
Merijn Schepers, 6504477, m.schepers2@students.uu.nl

The features included in this ray tracer are:
- 3D
- Reflections
- Circular area lights (to prevent hard shadows)
- Spheres and Planes
- Separate coordinate system
- Multithreading
- Anti-aliasing in the form of Supersampling (SSAA)
- Optimized calculations with normals

The ray tracer is operated in the following way:
- Upon startup, you can fill in some information about the rendering in the console application
- Here you can adjust things like resolution, SSAA and which scene you want to see
- Then just start the application and see how the scene gets rendered

We recommend the following settings for the two scenes we have prepared:
Scene 1 (simple):
- Top down to make it 2D
- Resolution: 1500x1000
- SSAA: 2
- No advanced settings

Scene 2 (advanced):
- 3D
- Resolution: 1500x1000
- SSAA: 2
- No advanced settings

The first scene is a simple top down view of a few spheres in a room. Some spheres are partially reflective.
The second scene is a 3D scene that uses two mirrors opposite of each other to create a cool looking room.
The second scene also uses area lights to prevent hard shadows.

If you want to add objects, you have to go to the Program.cs file and look for the following two functions:
- 'void AddSimpleObjects()' for the simple scene
- 'void AddAdvancedObjects()' for the advanced scene

You can use the functions that we have used there to add or adjust the current objects.
You can also go to the advanced settings in the console to tinker some more.

In the advanced settings you can adjust the following settings:
- Screen z value, determines the FOV and zoom of the camera
- Camera z value, determines the FOV and zoom of the camera
- 'Light density', determines how many sublights an area light has. The higher this number, the longer the scene will take to render, 
   but also the more advanced the shadows are.
- 'Maximum bounces', determines the maximum number of bounces a ray can make between two or more reflective objects (to prevent stackoverflows)

We used the book for the diffuse shading, as well as the slides for the reflections.