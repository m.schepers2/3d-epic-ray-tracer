﻿using OpenTK;
using System;

class PointLight : Light
{
    public PointLight(float _strength, Vector3 _colour, Vector3 _position)
        : base(_strength, _position, _colour)
    {
        sources = GetPoints();
    }

    public Vector3[] GetPoints()
    {
        return new Vector3[1] { position };
    }
}

