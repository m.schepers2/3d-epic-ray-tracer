﻿using OpenTK;
using System;

struct Cube : Primitive
{
    public Material mat;
    public Plane[,] pairs;

    public Cube(Vector3 min, Vector3 max, Material _mat)
    {
        pairs = new Plane[3, 2];
        //x=0 planes
        pairs[0, 0] = new Plane(new Vector3(0, max.Y - min.Y, 0), new Vector3(0, 0, max.Z - min.Z), min, _mat);
        pairs[0, 1] = new Plane(new Vector3(0, max.Y - min.Y, 0), new Vector3(0, 0, max.Z - min.Z), max, _mat);
        //y=0 planes
        pairs[1, 0] = new Plane(new Vector3(max.X - min.X, 0, 0), new Vector3(0, 0, max.Z - min.Z), min, _mat);
        pairs[1, 1] = new Plane(new Vector3(max.X - min.X, 0, 0), new Vector3(0, 0, max.Z - min.Z), max, _mat);
        //z=0 planes
        pairs[2, 0] = new Plane(new Vector3(max.X - min.X, 0, 0), new Vector3(0, max.Y - min.Y, 0), min, _mat);
        pairs[2, 1] = new Plane(new Vector3(max.X - min.X, 0, 0), new Vector3(0, max.Y - min.Y, 0), max, _mat);

        mat = _mat;
    }
    
    public void Intersects(Ray ray)
    {
    }
}
