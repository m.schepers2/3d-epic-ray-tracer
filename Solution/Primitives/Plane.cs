﻿using OpenTK;
using System;

struct Plane : Primitive
{
    public Vector3 u;
    public Vector3 v;
    public Vector3 offset;
    Vector3 point;
    Vector3 normal;
    public Material mat;

    public Plane(Vector3 _u, Vector3 _v, Vector3 _offset, Material _mat)
    {
        u = _u.Normalized();
        v = _v.Normalized();
        mat = _mat;
        offset = _offset;
        point = offset;
        normal = Vector3.Cross(v, u);
    }

    public void Intersects(Ray ray)
    {
        float denom = Vector3.Dot(normal, ray.direction);
        float t = Vector3.Dot(point - ray.origin, normal) * (1f / denom);
        if ((t >= 0) && (t < ray.distance || ray.distance < 0))
        {
            ray.distance = t;

            ray.mat = mat;
            ray.mat.colour = GetTexture(ray.GetPoint(t));
            ray.normal = normal;
        }
        
    }

    Vector3 GetTexture(Vector3 point)
    {
        float uP = Vector3.Dot(point + new Vector3(2005f), u);
        float vP = Vector3.Dot(point + new Vector3(200f), v);

        float texture = ((int)(2 * uP) + (int)vP) & 1;

        return new Vector3(texture, texture, texture) * mat.colour;
    }
}