﻿using OpenTK;
using System;

class Ray
{
    public Vector3 origin;
    public Vector3 direction;

    public float distance;
    public Vector3 normal;
    public Material mat;
    public int bounce;

    public Ray(Vector3 _origin, Vector3 _direction){

        origin = _origin;
        direction = _direction.Normalized();
        
        this.distance = -1;
    }

    public Vector3 GetPoint(float d)
    {
        return origin + direction * d;
    }

    public Vector3 GetGroundPoint()
    {
        float t = -1 * (origin.Z / direction.Z);
        return GetPoint(t);
    }
}