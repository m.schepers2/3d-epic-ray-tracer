# 3D Epic Ray Tracer

The features included in this ray tracer are:
- 3D
- Reflections
- Circular area lights (to prevent hard shadows)
- Spheres and Planes
- Separate coordinate system
- Multithreading
- Anti-aliasing in the form of Supersampling (SSAA)
- Optimized calculations with normals
