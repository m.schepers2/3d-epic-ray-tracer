﻿using OpenTK;
using System;

class StartUI
{
    public enum SceneType
    {
        Simple,
        Advanced
    };

    public Vector2 resolution;
    public int superSampling;
    public int maxBounces = 15;
    public float screenZ = 4;
    public float cameraZ = 6.5f;
    public int lightDensity = 3;
    public SceneType scene;

    public void Start()
    {
        scene = GetSceneType();
        resolution = GetResolution();
        superSampling = GetSuperSampling();

        if (GetAdvancedOptions())
        {
            Console.WriteLine("The next two options determine the zoom of the camera and the FOV.");
            cameraZ = GetCameraZ();
            screenZ = GetScreenZ();

            Console.WriteLine("The next two options determine the detail of the shadows and reflections.");
            lightDensity = GetLightDensity();
            maxBounces = GetMaxBounces();
        }
        
        Console.WriteLine("We will now start rendering the scene. Sit back and relax!");
    }

    SceneType GetSceneType()
    {
        Console.WriteLine("We have prepared 2 scenes: a simple 2d top down view and a more advanced cool looking 3d view.");
        Console.Write("Which one do you wish to see (type 'simple' or 'advanced'): ");

        string text = Console.ReadLine();
        switch (text)
        {
            case "simple":
                return SceneType.Simple;
            case "advanced":
                return SceneType.Advanced;
            default:
                Console.WriteLine("Please type 'simple' or 'advanced' without the parentheses!");
                return GetSceneType();
        }
    } 

    Vector2 GetResolution()
    {
        Console.Write("Insert Screen Resolution (e.g. 1920x1080): ");
        try
        {
            string[] split = Console.ReadLine().Split('x');
            return new Vector2(int.Parse(split[0]), int.Parse(split[1]));
        }
        catch
        {
            Console.WriteLine("Please use the right format as seen in the example!");
            return GetResolution();
        }
    }

    int GetSuperSampling()
    {
        Console.Write("Insert supersampling strength in powers of 2 (e.g. 2, 4, 8, 16): ");
        
        try
        {
            string text = Console.ReadLine();
            int x = int.Parse(text);

            if((x != 0) && ((x & (x - 1)) == 0))
            {
                return x;
            }
            else
            {
                Console.WriteLine("Please insert a power of 2 for supersampling!");
                return GetSuperSampling();
            }
        }
        catch
        {
            Console.WriteLine("Please insert a number!");
            return GetSuperSampling();
        }
    }

    bool GetAdvancedOptions()
    {
        Console.Write("Do you want to tinker with the more advanced settings (type 'y' or 'n'): ");

        string text = Console.ReadLine();
        switch (text)
        {
            case "y":
                return true;
            case "n":
                return false;
            default:
                Console.WriteLine("Please write 'y' or 'n' without the parentheses!");
                return GetAdvancedOptions();
        }
    }

    float GetCameraZ()
    {
        Console.Write("Insert the Z value of the camera (e.g. 6.9 or 4,20): ");
        try
        {
            string text = Console.ReadLine();
            return float.Parse(text);
        }
        catch
        {
            Console.WriteLine("Please insert a number in the right format using a '.' or ','!");
            return GetCameraZ();
        }
    }

    float GetScreenZ()
    {
        Console.Write("Insert the Z value of the screen (e.g. 6.9 or 4,20): ");
        try
        {
            string text = Console.ReadLine();
            return float.Parse(text);
        }
        catch
        {
            Console.WriteLine("Please insert a number in the right format using a '.' or ','!");
            return GetScreenZ();
        }
    }

    int GetLightDensity()
    {
        Console.Write("Insert the amount of detail the area lights should have (recommended between 2 and 6): ");
        try
        {
            string text = Console.ReadLine();
            return int.Parse(text);
        }
        catch
        {
            Console.WriteLine("Please insert a number!");
            return GetLightDensity();
        }
    }

    int GetMaxBounces()
    {
        Console.Write("Insert the maximum amount of bounces reflection rays (mirrors) can make (recommended between 10 and 30): ");
        try
        {
            string text = Console.ReadLine();
            return int.Parse(text);
        }
        catch
        {
            Console.WriteLine("Please insert a number!");
            return GetMaxBounces();
        }
    }

}
