using OpenTK;
using System.Collections.Generic;
using System;
using System.Threading;

class Program
{
    public Screen screen;
    FakeScreen highResScreen;

    IList<Primitive> primitives;
    IList<Primitive> lightPrimitives;
    IList<Light> lights;

    public Surface surface { get => screen.surface; }

    Vector3 camera;
    int xalias;
    int maxBounces;
    int lightDensity;
    StartUI.SceneType scene;

    public Program(StartUI startData)
    {
        scene = startData.scene;
        camera = new Vector3(0f, 0f, startData.cameraZ);
        xalias = startData.superSampling;
        maxBounces = startData.maxBounces;
        lightDensity = startData.lightDensity;
    }

    public void Init()
    {
        primitives = new List<Primitive>();
        lightPrimitives = new List<Primitive>();
        lights = new List<Light>();
        
        highResScreen = new FakeScreen(new Vector2(surface.width * xalias, surface.height * xalias), screen.z, screen.size.X);

        switch (scene)
        {
            case StartUI.SceneType.Advanced:
                AddAdvancedObjects();
                break;
            case StartUI.SceneType.Simple:
                AddSimpleObjects();
                break;
        }

        StartTracing();
    }

    void AddSimpleObjects()
    {
        //Plane
        AddPlane(new Vector3(1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, -2f), new Vector3(255, 255, 255), 0f);
        AddPlane(new Vector3(-1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, camera.Z + 1), new Vector3(0, 0, 0), 1f);

        //Walls
        AddPlane(new Vector3(1, 0, 0), new Vector3(0, 0, 1), new Vector3(0, 12, 0), new Vector3(200, 200, 200));
        AddPlane(new Vector3(0, 0, 1), new Vector3(1, 0, 0), new Vector3(0, -12, 0), new Vector3(200, 200, 200));
        AddPlane(new Vector3(0, 1, 0), new Vector3(0, 0, 1), new Vector3(-12, 0, 0), new Vector3(200, 200, 200));
        AddPlane(new Vector3(0, 0, 1), new Vector3(0, 1, 0), new Vector3(12, 0, 0), new Vector3(200, 200, 200));

        //Objects
        AddSphere(new Vector3(0, 0, -2), 2, new Vector3(100, 100, 100), .8f);
        AddSphere(new Vector3(4, 0, -2), 1, new Vector3(255, 100, 100), .2f);
        AddSphere(new Vector3(0, 4, -2), 1, new Vector3(100, 255, 100), .2f);
        AddSphere(new Vector3(-4, 0, -2), 1, new Vector3(100, 100, 255), .2f);
        AddSphere(new Vector3(0, -4, -2), 1, new Vector3(255, 100, 100), .2f);

        //Lights
        AddPointLight(2, new Vector3(255, 255, 0), new Vector3(-3, -3, 0));
        AddPointLight(2, new Vector3(0, 255, 255), new Vector3(3, -3, 0));
        AddPointLight(2, new Vector3(255, 0, 255), new Vector3(-3, 3, 0));
        AddPointLight(2, new Vector3(255, 255, 255), new Vector3(3, 3, 0));
    }

    void AddAdvancedObjects()
    {
        //Mirrors
        AddPlane(new Vector3(1, 0, -.05f), new Vector3(0, 1, 0), new Vector3(0, 0, -2f), new Vector3(0, 0, 0), .9f);
        AddPlane(new Vector3(-1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, 0, 7), new Vector3(0, 0, 0), .9f);

        //Walls
        AddPlane(new Vector3(1, 0, 0), new Vector3(0, 0, 1), new Vector3(0, 3, 0), new Vector3(200, 200, 200), 0f);
        AddPlane(new Vector3(0, 0, 1), new Vector3(1, 0, 0), new Vector3(0, -3, 0), new Vector3(200, 200, 200), 0f);
        AddPlane(new Vector3(0, 1, 0), new Vector3(0, 0, 1), new Vector3(-3, 0, 0), new Vector3(200, 200, 200), 0f);
        AddPlane(new Vector3(0, 0, 1), new Vector3(0, 1, 0), new Vector3(3, 0, 0), new Vector3(200, 200, 200), 0f);

        //Objects
        AddSphere(new Vector3(1, 0, 1), .5f, new Vector3(50, 50, 50), .8f);
        AddSphere(new Vector3(0, 2, 7), 1f, new Vector3(50, 50, 200));
        AddSphere(new Vector3(-1, 1, 3), .3f, new Vector3(255, 255, 20), .1f);

        //Lights
        AddPointLight(2, new Vector3(50, 50, 200), new Vector3(2, 0, 5));
        AddRoundLight(new Vector3(-2, 0, 1), .1f, 500, new Vector3(200, 50, 200));
    }

    void AddSphere(Vector3 position, float radius, Vector3 colour, float reflectiveness = 0)
    {
        primitives.Add(new Sphere(position, radius, new Material(colour, reflectiveness)));
    }

    void AddPlane(Vector3 u, Vector3 v, Vector3 offset, Vector3 colour, float reflectiveness = 0)
    {
        primitives.Add(new Plane(u, v, offset, new Material(colour, reflectiveness)));
    }

    void AddPointLight(int strength, Vector3 colour, Vector3 position)
    {
        lights.Add(new PointLight(strength, colour, position));
    }

    void AddRoundLight(Vector3 position, float radius, int strength, Vector3 colour)
    {
        Sphere sphere = new Sphere(position, radius, new Material(colour, true));
        lights.Add(new RoundLight(strength, sphere, lightDensity));
        lightPrimitives.Add(sphere);
    }

    public void Tick()
    {
    }

    void StartTracing()
    {
        ThreadCounter counter = new ThreadCounter();
        
        for (int i = 0; i < Environment.ProcessorCount - 1; i++)
        {
            new Thread(RayTrace).Start(counter);
        }
    }

    void RayTrace(object data)
    {
        ThreadCounter counter = data as ThreadCounter;

        while (counter.count < highResScreen.Width * highResScreen.Height)
        {
            int index = counter.count++;

            if (index >= highResScreen.Width * highResScreen.Height)
                return;

            int x = index % highResScreen.surface.width;
            int y = index / highResScreen.surface.width;

            Vector3 pixel = highResScreen.GetPixel(x, y);
            Vector3 direction = pixel - camera;
            Ray ray = new Ray(camera, direction);

            highResScreen.SetPixel(x, y, ShootRay(ray));

            if(x != 0 && y != 0 && x % xalias == 0 && y % xalias == 0)
            {
                SetAveragePixel(x - xalias, y - xalias);
            }
        }
    }

    Vector3 ShootRay(Ray ray)
    {
        foreach (Primitive p in primitives)
        {
            p.Intersects(ray);
        }
        foreach (Primitive l in lightPrimitives)
        {
            l.Intersects(ray);
        }

        Vector3 diffuseColor = Vector3.Zero;

        if (ray.distance >= 0)
        {
            if (!ray.mat.light)
            {
                if (ray.mat.specular > 0 && ray.bounce < maxBounces)
                {
                    Vector3 R = ray.direction - 2f * ray.normal * Vector3.Dot(ray.normal, ray.direction);
                    Vector3 origin = ray.GetPoint(ray.distance) + R * 0.001f;
                    Ray reflectRay = new Ray(origin, R);
                    reflectRay.bounce = ray.bounce + 1;

                    diffuseColor += ShootRay(reflectRay) * ray.mat.specular;
                }

                if (ray.mat.specular < 1)
                {
                    diffuseColor += GetLightInfluence(ray) * (1f - ray.mat.specular);
                }
            }
            else
            {
                diffuseColor = ray.mat.colour * (4f / 3f);
            }
        }

        return diffuseColor;
    }

    bool ShootShadowRay(Ray ray, float distance)
    {
        foreach (Primitive p in primitives)
        {
            p.Intersects(ray);
            if (ray.distance < distance - .0001f && ray.distance != -1)
                return false;
        }
        return true;
    }

    Vector3 GetLightInfluence(Ray ray)
    {
        Vector3 newColor = Vector3.Zero;

        foreach (Light light in lights)
        {
            for(int i = 0; i < light.sources.Length; i++)
            {
                newColor += HandleShadowRay(ray, light.sources[i], light);
            }
        }

        return newColor;
    }

    Vector3 HandleShadowRay(Ray ray, Vector3 lightpoint, Light light)
    {
        Vector3 Ldirection = ray.GetPoint(ray.distance) - lightpoint;
        float dot = Vector3.Dot(ray.normal, Ldirection);

        if(dot > 0)
        {
            Ray shadowRay = new Ray(lightpoint, Ldirection);

            if (ShootShadowRay(shadowRay, Ldirection.Length))
            {
                return light.GetDiffuseShading(dot, Ldirection.Length, ray.mat.colour);
            }
        }

        return Vector3.Zero;
    }

    void ConvertScreen()
    {
        int scale = highResScreen.surface.width / screen.surface.width;

        for (int x = 0; x < highResScreen.surface.width; x += scale)
        {
            for (int y = 0; y < highResScreen.surface.height; y += scale)
            {
                Vector3 average = Vector3.Zero;

                for (int xoff = 0; xoff < scale; xoff++)
                {
                    for (int yoff = 0; yoff < scale; yoff++)
                    {
                        average += highResScreen.GetColour(x + xoff, y + yoff);
                    }
                }

                average *= (1.0f / (scale * scale));

                screen.SetPixel(x / scale, y / scale, average);
            }
        }
    }

    void SetAveragePixel(int x, int y)
    {
        Vector3 average = Vector3.Zero;

        for (int xoff = 0; xoff < xalias; xoff++)
        {
            for (int yoff = 0; yoff < xalias; yoff++)
            {
                average += highResScreen.GetColour(x + xoff, y + yoff);
            }
        }

        average *= (1.0f / (xalias * xalias));

        screen.SetPixel(x / xalias, y / xalias, average);
    }
}
