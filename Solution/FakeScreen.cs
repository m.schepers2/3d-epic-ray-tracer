﻿using OpenTK;
using System;

class FakeScreen : Screen
{
    Vector3[] fakeScreen;

    public FakeScreen(Vector2 _resolution, float _z, float _width)
        : base(_resolution, _z, _width)
    {
        fakeScreen = new Vector3[(int)(_resolution.X * _resolution.Y)];
    }

    public override void SetPixel(int _x, int _y, Vector3 _colour)
    {
        fakeScreen[_x + _y * surface.width] = _colour;
    }

    public Vector3 GetColour(int _x, int _y)
    {
        return fakeScreen[_x + _y * surface.width];
    }
}
