﻿using OpenTK;

interface Primitive
{ 
    void Intersects(Ray ray);
}