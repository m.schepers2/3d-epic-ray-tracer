﻿using System;
using OpenTK;

class PlaneLight : Light
{
    Plane plane;
    Vector2 step;
    Vector2 density;

    public PlaneLight(Plane _plane, float _strength, Vector2 _size, Vector2 _density)
        : base(_strength * (_density.X * _density.Y), _plane.offset, _plane.mat.colour)
    {
        step = new Vector2();
        step.X = _size.X / _density.X;
        step.Y = _size.Y / _density.Y;
        plane = _plane;
        density = _density;
        sources = GetPoints();
    }

    public Vector3[] GetPoints()
    {
        Vector3[] points = new Vector3[(int)(density.X * density.Y)];
        
        for(int x = 0; x < density.X; x++)
        {
            for(int y = 0; y < density.Y; y++)
            {
                Vector2 percentages = new Vector2(x, y) * step;
                Vector3 point = plane.u * percentages.X + plane.v * percentages.Y;
                points[x + y * (int)density.X] = point + plane.offset;
            }
        }

        return points;
    }
}