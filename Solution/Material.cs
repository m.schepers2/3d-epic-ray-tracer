﻿using OpenTK;

struct Material
{
    public Vector3 colour;
    public float specular;
    public bool light;

    public Material(Vector3 _colour, bool _light = false)
    {
        colour = _colour * (1f / 255f);
        specular = 0;
        light = _light;
    }

    public Material(Vector3 _colour, float _specular, bool _light = false)
    {
        colour = _colour * (1f / 255f);
        specular = _specular;
        light = _light;
    }
}