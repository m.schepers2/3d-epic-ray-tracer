﻿using OpenTK;
using System;

struct Sphere : Primitive
{
    public Vector3 origin;
    public float radius;
    public float radius2;
    public Material mat;

    public Sphere(Vector3 _origin, float _radius, Material _mat)
    {
        origin = _origin;
        radius = _radius;
        radius2 = _radius * _radius;
        mat = _mat;
    }

    //returns the smallest value, -1f if there is no intersection;
    public void Intersects(Ray ray)
    {
        Vector3 c = origin - ray.origin;
        float t = Vector3.Dot(c, ray.direction);
        Vector3 q = c - t * ray.direction;
        float psquared = Vector3.Dot(q, q);
        if (psquared > radius2)
            return;
        t -= (float)Math.Sqrt(radius2 - psquared);

        if ((t < ray.distance || ray.distance < 0) && (t > 0))
        {
            ray.distance = t;
            ray.mat = mat;

            Vector3 intersectPoint = ray.GetPoint(t);
            ray.normal = (origin - intersectPoint) / radius;            
        }
    }
}
