﻿using OpenTK;
using System;

class RoundLight : Light
{
    Sphere sphere;
    int density;

    public RoundLight(float _strength, Sphere _sphere, int _density)
        : base(_strength / (float)(_density * _density), _sphere.origin, _sphere.mat.colour)
    {
        sphere = _sphere;
        density = _density;
        sources = GetPoints();
    }

    public Vector3[] GetPoints()
    {
        Vector3[] points = new Vector3[density * density];
        for(int i = 1; i <= density; i++)
        {
            for(int j = 1; j <= density; j++)
            {
                double theta = (360 / i) * 180 / 3.141592;
                double phi = (360 / j) * 180 / 3.141592;

                Vector3 point = new Vector3();
                point.X = (float)(sphere.origin.X + sphere.radius * Math.Sin(theta) * Math.Cos(phi));
                point.Y = (float)(sphere.origin.Y + sphere.radius * Math.Sin(theta) * Math.Sin(phi));
                point.Z = (float)(sphere.origin.Z + sphere.radius * Math.Cos(theta));

                points[i - 1 + (j - 1) * density] = point;
            }

        }

        return points;
    }
}

