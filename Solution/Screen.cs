﻿using OpenTK;
using System;

class Screen
{
    public Surface surface;
    public float z;
    public Vector2 size;
    Vector2 halfSize;

    public float Width { get => surface.width; }
    public float Height { get => surface.height; }

    public Screen(Vector2 _resolution, float _z, float _width)
    {
        surface = new Surface((int)_resolution.X, (int)_resolution.Y);
        z = _z;
        size = new Vector2(_width, _width / (Width / Height));
        halfSize = size / 2f;
    }

    //returns the coordinates of a pixel in the current coor system
    public Vector3 GetPixel(int _x, int _y)
    {
        float x = _x * size.X * (1f / Width) - halfSize.X;
        float y = _y * size.Y * (1f / Height) - halfSize.Y;
        return new Vector3(x, y, z);
    }

    //sets the colour of a pixel
    public virtual void SetPixel(int _x, int _y, Vector3 _colour)
    {
        //convert colour to lower quality colour in int
        _colour *= 255f;
        int red = Math.Min((int)_colour.X, 255);
        int green = Math.Min((int)_colour.Y, 255);
        int blue = Math.Min((int)_colour.Z, 255);
        surface.pixels[_x + _y * surface.width] = MixColour(red, green, blue);
    }

    public int MixColour(int red, int green, int blue)
    {
        return (red << 16) + (green << 8) + blue;
    }
}