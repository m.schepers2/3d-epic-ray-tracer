﻿using OpenTK;
using System;

abstract class Light
{
    public float strength;
    public Vector3 position;
    public Vector3 colour;
    public Vector3[] sources;

    public Light(float _strength, Vector3 _position, Vector3 _colour)
    {
        strength = _strength;
        position = _position;
        colour = _colour / 255f;
    }

    public virtual Vector3 GetDiffuseShading(float dot, float r, Vector3 MColour)
    {
        return dot * (1 / (r * r)) * strength * colour * MColour;
    }
}
